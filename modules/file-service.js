const fs = require("fs");
const path = require("path");

const fspromise = require("fs/promises");

let fileService = {
    readCallBack : (filename) => {
        const file = path.resolve(process.cwd(), 'data', filename);
        //process.cwd() => chemin jusque NODEJS_DEMOFILESYSTEM (celui dans lequel on est en train de travailler)
        
        //fs.readFile(`../data/${filename}`, (error, data) => {});
        fs.readFile(file, (error, buffer) => {
            console.log("ReadCallBack : Demande de lecture");
            if(error){
                console.log(error);
                return;
            }
            console.log("ReadCallBack : ", buffer);
            const data = buffer.toString('utf-8');
            console.log("ReadCallBack :" , data);
        })
    }, 

    readPromise : (filename) => {
        const file = path.resolve(process.cwd(), 'data', filename);

        fspromise.readFile(file, {encoding : 'utf-8'})
            .then(buffer => {
                console.log('Buffer' ,buffer);
            }).catch((error) => {
                console.log("Une erreur est survenue")
            })
    },

    openCallBack : function(filename) {
        const file = path.resolve(process.cwd(), 'data', filename);
        const buffer  = Buffer.alloc(128);

        fs.open(file, 'rs+', (error, fd) => {
            console.log("openCallBack : Ouverture du fichier");
            if(error){
                console.log("Boum !");
                return;
            }

            //fd -> fichier obtenu à l'ouverture
            //buffer -> buffer crée qui va contenir le buffer
            //offset -> 0 -> Position dans le buffer à laquelle on va commencer
            //length  -> Combien de bytes vont être encodés -> 4, donc 4lettres
            //position -> Position dans le fichier texte -> 3, donc on commence à la 4ème lettre puisque les chaines de caractères comme les tableaux commencent à 0
            fs.read(fd, buffer, 0, 4, 3, () => {
                console.log('openCallBack : Lecture du fichier');
                console.log('openCallBack Buffer : ', buffer);
                const data = buffer.toString('utf-8');
                console.log('openCallBack Data lecture : ', data);
            })
            // fs.write(fd, buffer, 0, buffer.length, 0, () => {

            // });
            //Ferme le fichier ouvert
            fs.close(fd, (error) => {})
        });
        
    },

    openPromise : async (filename) => {
        const file = path.resolve(process.cwd(), 'data', filename);
        const buffer = Buffer.alloc(128);

        const fileHandler = await fspromise.open(file, 'r');
        await fileHandler.read(buffer, 0,buffer.length, 0);
        console.log(buffer.toString('utf-8'));

    },

    writeCallBack : function(filename) {
        const file = path.resolve(process.cwd(), 'data', filename);

        const data = `Bonjour, nous sommes le ${new Date().toLocaleDateString()}`;
        // const data = "Pouet pouet";
        fs.writeFile(file, data, {encoding : 'utf-8', flag : 'ax'}, (error) => {
            if(error){
                console.log("Boum !");
                return;
            }
            console.log("C'est bon :)")

        })
    },

    writePromise : (filename) => {
        const file = path.resolve(process.cwd(), 'data', filename);
        const data = `Bonjour, nous sommes le ${new Date().toLocaleDateString()}`;

        fspromise.writeFile(file, data, { encoding : 'utf-8', flag : 'w'})
            .then(() => {
                console.log("C'est écrit !");
            }).catch(() => {
                console.log("Boum !");
            })
    },

    deleteFile : (filename) => {
        const file = path.resolve(process.cwd(), 'data', filename);
        fs.unlink(file, (error) => {
            if(error){
                console.log("Ono");
            }
        })
    },

    readDirectory : (dirname) => {
        const dir = path.resolve(process.cwd(), dirname);
        fs.readdir(dir, (error, files) => {
            files.forEach(fichier => console.log(fichier));
        })

    }

}

module.exports = fileService;

//Lien vers la doc
//https://nodejs.org/api/fs.html