const fileService = require("./modules/file-service");

console.log("Demo Node JS - File System");
// fileService.readCallBack("demoread.txt");
// fileService.openCallBack("demoread.txt");
//fileService.writeCallBack("bonjour.txt");

//Suppression
//fileService.writeCallBack("test.txt");
fileService.deleteFile("test.txt");

fileService.readDirectory("data");


// fileService.readPromise("demoread.txt");
fileService.openPromise("demoread.txt");
fileService.writePromise("bonjourPromise.txt");